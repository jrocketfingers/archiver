/**
 * @brief  Memory allocating functions
 *
 * Contains functions for memory manipulation. These are used by
 * both Huffman and LZW algorithms for better usage of memory over
 * macros described in memalloc/memalloc.h.
 */

 /**
 * @file "memalloc/memalloc.c"
 * @author Nikola Kolevski
.*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
* @brief Allocates memory buffer of n_item elements, each using item_size of memory space.
*
* @param item_size Size of one element
* @param n_item Number of elements
*
* @return data pointer of third element in allocated memory
*
* Function allocate (n_item + 2) elements in memory. First one is for size of item,
* second one is number of all items ( n_item ), and last n_item elements
* are at the moment filled with junk, but will be used later.
* Purpose of 2 "hidden" elements is that you can see the length of memory block in constant
* time.
*
*/

void* mem_alloc(size_t item_size, size_t n_item)
{
    size_t *x = calloc(1, sizeof(size_t)*2 + n_item * item_size);

    /* fill the two extra hidden size_t entries with item size and the
     * number of items. */
    x[0] = item_size;
    x[1] = n_item;

    /* return the data pointer, the user probably knows how to handle it. */
    return x + 2;
}

/**
* @brief Reallocates void pointer m to another number of elements
*
* @param m data pointer that needs to be reallocated
* @param new_n new size
*
* @return data pointer that now points to extended memory buffer
*
* Function copies old data, reallocate space to different number of elements,
* pastes the old data starting from third element, and the left stays empty.
* The first two keeps their functions for item_size and n_item, but updated n_item.
*
*/


void* mem_extend(void *m, size_t new_n)
{
    /* fetch the info pointer */
    size_t *x = (size_t*)m - 2;

    /* new_n doesn't account for the info space, so we have to add it again */
    x = realloc(x, sizeof(size_t) * 2 + *x * new_n);

    /* if we're expanding, we've gotta clear the trailing space */
    if(new_n > x[1])
        memset((char*)(x+2) + x[0] * x[1], 0, x[0] * (new_n - x[1]));

    x[1] = new_n;

    return x + 2;
}

/**
*
* @brief setting allocated memory to all zeros
*
* @param m data pointer
*
* @return void
*
*/


inline void _clear(void *m)
{
    size_t *x = (size_t*)m - 2;
    memset(m, 0, x[0] * x[1]);
}
