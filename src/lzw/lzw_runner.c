#include <stdio.h>
#include "lzw.h"
#include "memalloc.h"

int main(int argc, char **argv)
{
    FILE *input, *output;
    byte *in, *out, *dout;
    size_t size, rsize;

    input = fopen(argv[1], "rb");
    output = fopen(argv[2], "wb");

    fseek(input, 0, SEEK_END);
    size = ftell(input);
    rewind(input);

    in = _new(byte, size);
    rsize = fread(in, 1, size, input);

    out = lzw_encode(in, 16);
    dout = lzw_decode(out);
    fwrite(dout, 1, _len(dout), output);

    fclose(input);
    fclose(output);
    _del(in);
    _del(out);

    return 0;
}
