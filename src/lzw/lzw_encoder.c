/**
 * @brief LZW encoder procedure source
 * @file lzw_encoder.c
 * @author Nikola Kolevski
 */
#include <stdio.h>
#include "lzw.h"
#include "memalloc.h"

/** \brief LZW encoding procedure
 *
 * \param in byte* Input data block
 * \param max_bits int Maximum encoding block
 * \return byte* Output data block (compressed)
 *
 * The specifics of the procedure are in the table storage.
 * The table represents a vectorized DAG. By storing an array of 256
 * index-pointers we can traverse the graph as we approach the appropriate
 * sequence that exists in the graph. Although the graph contains all of the
 * edges, it's index-pointers, or vertices containing codes may be non existent.
 * If the algorithm reaches such a vertex. That means that the current sequence
 * is not currently present in the table and should be added for the next
 * iteration to use. It is assigned a code that is simply the next code in a sequence.
 * this allows us to know when we exceeded the necessary amount of bits to represent
 * the current code. Knowing this, we can do variable encoding by extending our
 * codelength every time we hit a square of two (and henceforth need an additional bit
 * to proceed. It's maximum code length is limited by the max_bits parameter. This is
 * used to achieve better performing encoding for shorter codelengths in specific use cases.
 *
 * In terms of data returned it is pretty straightforward. It returns a memalloced
 * block of data understandable by LZW decoder. Due to using the memalloc module, it
 * needs not to pass its length, but solely the data pointer.
 */
byte* lzw_encode(byte *in, int max_bits)
{
    size_t progress = _len(in);  // pr
    size_t size = _len(in);
    int bits = 9,x,prevx = 0,i;           // current write-out size
    int next_shift = 512;   // current maximum code (2^9)
    ushort code, c, nc, next_code = M_NEW;
    lzw_enc_t *d = _new(lzw_enc_t, 512);

    if(max_bits > 15) max_bits = 15;
    if(max_bits < 9) max_bits = 12;

    byte *out = _new(ushort, 4);

    int out_len = 0, buffer_bits = 0;
    uint32_t buffer = 0;

    inline void write_bits(ushort data)
    {
        buffer = (buffer << bits) | data;         // add the data on top of the buffer

        buffer_bits += bits;
        if(_len(out) <= out_len) _extend(out);
        while(buffer_bits >= 8) {
            buffer_bits -= 8;
            out[out_len++] = buffer >> buffer_bits;
            buffer &= (1 << buffer_bits) - 1;           // clear already printed bits
        }
    }

    /* initial entry is used as a code straight away */
    code = *(in++);
    //printf("Progress Bar: [");

    /* then we're sweeping the rest of the file.
     prefix increment is there solely because we've already taken a chunk
     of the input file. */

    while(--progress) {
        x = (100*(size - progress))/size;
        if(x != prevx) {
            printf("Progress Bar: [");
            for(i=0;i < 25;i++)
                if(i < x/4)
                    printf("#");
                else
                    printf("-");
            printf("]%3d%%\r",x);
        }

        prevx = x;

        c = *(in++);
        if((nc = d[code].next[c]))
            code = nc;
        else {
            write_bits(code);
            nc = d[code].next[c] = next_code++;
            code = c;
        }

        if(next_code == next_shift) {
            if(++bits > max_bits) {
                write_bits(M_CLR);

                bits = 9;
                next_shift = 512;
                next_code = M_NEW;
                _clear(d);
            } else
                _setsize(d, next_shift *= 2);
        }
    }

    write_bits(code);
    write_bits(M_EOD);

    if(buffer) write_bits(buffer);

    _del(d);

    _setsize(out, out_len);

    return out;
}
