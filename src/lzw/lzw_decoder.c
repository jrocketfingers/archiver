/**
 * @brief LZW decoder procedure source
 * @file lzw_decoder.c
 * @author Nikola Kolevski
 */
#include <stdio.h>
#include "lzw.h"
#include "memalloc.h"

/** \brief LZW decoder procedure.
 *
 * \param in byte* Encoded input data
 * \return byte* Decoded output data
 *
 * The jist of the decoder's complexity is in it's tracebacking through a similar graph.
 * The reconstruction process relies on the fact that LZW leaves out some of the char codes
 * intact before it can build out its table. The algorithm is tracebacking down the back chain
 * until it hits an element from the char width-range. Once the char is found, it writes out its way
 * back, in the same manner encoder forms a chain and outputs the last code from the chain.
 */
byte *lzw_decode(byte *in) {
    byte *out = _new(byte, 4);
    int out_len = 0,x,size,i;

    inline void write_out(byte c) {
        while(out_len >= _len(out)) _extend(out);
        out[out_len++] = c;
    }

    ushort c, code, next_code, t;
    int bits = 9, buf_bits = 0, next_shift, j, len;
    uint32_t buffer = 0;

    lzw_dec_t *d = _new(lzw_dec_t, 512);

    inline void clear_table() {
        _clear(d);
        for(j = 0; j < 256; j++) d[j].c = j;
        next_code = M_NEW;
        next_shift = 512;
        bits = 9;
    }

    clear_table();
    size = len = _len(in);
    while(len) {
        x = (100*(size - len))/size;
        printf("Progress Bar: [");
        for(i=0;i < 25;i++)
            if(i < x/4)
                printf("#");
            else
                printf("-");
        printf("]%3d%%\r",x);
        /* read bits */
        while(buf_bits < bits) {
            if(len > 0) {
                buffer = (buffer << 8) | *(in++);
                buf_bits += 8;
                len--;
            } else {
                buffer = buffer << (bits - buf_bits); // pad with 0s
                buf_bits = bits;                // to the width of the expected
                                                // bitlength
            }
        }

        buf_bits -= bits;               // mark 'bits' bits as read
        code = buffer >> buf_bits;      // now buf_bits contains remaining
                                        // bits so we dump them out
        buffer &= (1 << buf_bits) - 1;  // now preserve teh remainder,
                                        // mask other bits
        /* END read bits */

        if(code == M_EOD) break;
        if(code == M_CLR) {
            clear_table();
            continue;
        }

        if(code >= next_code) {
            fprintf(stderr, "Corrupted data sequence.");
            _del(out);
            goto stop;
        }

        c = code;
        d[next_code].prev = code;   // we're certainly adding a new code, and the
                                    // one we've read must already exist.

        while(c > 255) {            // while c is not a character
            t = d[c].prev;          // get the previous one
            d[t].back = c;          // set the traceback
            c = t;                  // proceed to it. Common list iterration.
        }

        d[next_code - 1].c = c;
        while(d[c].back) {
            write_out(d[c].c);
            t = d[c].back;
            d[c].back = 0;          // clear out current code backtrace
            c = t;
        }
        write_out(d[c].c);

        if(++next_code >= next_shift) {
            if(++bits > 16) {
                fprintf(stderr, "Too many bits.\n");
                _del(out);
                goto stop;
            }
            _setsize(d, next_shift *= 2);
        }
    }

    if(code != M_EOD) fprintf(stderr, "Bits did not end in an EOD.\n");

    _setsize(out, out_len);
stop: _del(d);

    return out;
}
