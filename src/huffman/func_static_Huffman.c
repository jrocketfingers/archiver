/**
* @author Ivan Miljkovic
* @file "func_static_Huffman.c"
* @brief Huffman algorithm library
*
* This file contains functions of Huffman algorithm, both compressing and decompressing.
* Other functions call from this file only static_Huffman() and decompress_Huffman() for compressing or decompressing.
* All other functions, beside these two, are internal functions, and are unusable to any other function.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include "static_Huffman.h"

#include "memalloc.h"

#define BLOCK 32*1024

/* ======================== queue functions ======================== */
/**
* @brief insert in priority queue
* @param tree pointer to priority queue
* @param for_insert node for insert
* @return void
*
* Forming priority queue for creating Huffman binary tree.
*/

void insert(tree_node_t ** tree, tree_node_t *for_insert){
    tree_node_t *q = 0, *p;
    tree_node_t *new_node;
    p = *tree;
    new_node = (tree_node_t *) malloc(sizeof(tree_node_t));

    new_node->frec = for_insert->frec;
    new_node->ch = for_insert->ch;
    new_node->right = for_insert->right;
    new_node->left = for_insert->left;

    while ((p != 0) && (new_node->frec >= p->frec)){
        q = p;
        p = p->next;
    }
    if (q == 0){
        new_node->next = *tree;
        *tree = new_node;
    }
    else {
        new_node->next = q->next;
        q->next = new_node;
    }

}

/**
* @brief deleting node from priority queue
* @param tree pointer to priority queue
* @return void
*
* Deleting node with minimal frequency at the moment.
*/

tree_node_t * delete_queue(tree_node_t **tree)
{
    tree_node_t * new_node;
    new_node = *tree;
    if (new_node != 0){
        (*tree) = (*tree)->next;
        return new_node;
    }
    else
        return 0;
}

/**
* @brief freeing memory
* @param tree pointer to priority queue
* @return void
*
* Freeing memory used by priority queue or stack
*/

void free_struct(tree_node_t **tree){
    tree_node_t *old_node;
    old_node = *tree;

    while (old_node != 0){
        *tree = (*tree)->next;
        free(old_node);
        old_node = *tree;
    }
}

/* ===================== End of queue functions ===================== */

/* ======================== stack functions ======================== */

/**
* @brief push to stack
* @param stek stack pointer
* @param for_insert node to push
* @return void
*/

void push(tree_node_t ** stek, tree_node_t *for_insert)
{
    tree_node_t *new_node;

    new_node = (tree_node_t *) malloc(sizeof(tree_node_t));

    new_node = for_insert;

    if (*stek == 0)
        new_node->next = 0;
    else
        new_node->next = (*stek);
    *stek = new_node;
}

/**
* @brief pop from stack
* @param stek stack pointer
* @return popped node
*/


tree_node_t * pop(tree_node_t ** stek)
{
    tree_node_t *old_node;
    if ((*stek) != 0){
        old_node = *stek;
        *stek = (*stek)->next;
        return old_node;
    }
    else
        return 0;
}

/**
* @brief is stack empty
* @param stek stack pointer
* @return 1 if stack is empty, else 0
*/


int stek_empty(tree_node_t *stek){
    if (stek == 0)
        return 1;
    else
        return 0;
}

/* ===================== End of stack functions ===================== */

/* This function makes output files of character codes, and character replacement code. */
/* It also deallocate memory used for tree */
/* I have trouble with (not)portability of char **codes, so I did all the output in this function */

/**
* @brief makes compressed data buffer
* @param root root of Huffman binary tree
* @param stream pointer to beginning of data buffer
* @return pointer to compressed data buffer
*
* Makes output (compressed) data buffer of bytes codes in header and
* compressed data in main field. It reads Huffman codes for binary tree
* and puts them into matrix of bytes 256*max_tree_height, where current byte is in the index if matrix.
* Once that is done, function goes again from start to end of stream and replace current byte with
* its code using bit-writing. Bit-writing is separated into two cases. First one: the code is bigger than unused room in
* byte buffer. If so, fill unused room up and send it to output data, and put leftover to the beginning of the byte buffer.
* And second one: the code is not bigger then unused room in byte buffer, then just add it, and move on.
*
*/

BYTE * outputAndFree(tree_node_t **root, BYTE * stream)
{
    tree_node_t *stek = 0, *old_node;
    int i;
    BYTE ** codes;
    push(&stek, (*root));

    BYTE * output = _new(BYTE, 2*BLOCK);

    uint64_t output_index = 0, block_length_index, data_index;

    codes = (BYTE **) _new(BYTE*, 256);

    int height = treeHeight(*root);

    for (i = 0; i < 256; i++)
    {
        codes[i] = _new(BYTE, (height + 1));
        codes[i][0] = '\0';
    }

    while(!stek_empty(stek))
    {

        tree_node_t *next = pop(&stek);

        while(next != 0)
        {
            if ((next->left == 0) && (next->right == 0))
            {
                //printf("%c, %s\n", next->ch, next->code);
                strcpy(codes[next->ch], next->code);

                output[output_index++] = next->ch;
                for ( i = 0; i < strlen(next->code); i++)
                    output[output_index++] = next->code[i];
                output[output_index++] = '\n';
            }
            if (next->right != 0)
                push(&stek, next->right);

            old_node = next;
            next = next->left;
            free(old_node);
        }

    }

    //fprintf(output, "%c%c\n", 255, 255); // sign for end of table

    output[output_index++] = 255;
    output[output_index++] = 255;

    free_struct(&stek);

    uint64_t bytes = 0, bits_left = 0;

    int c;

    bits_left = bytes % 8;      // number of usable bits in last byte
    bytes /= 8;                 // number of bytes in compressed file
                                // These are needed to be written into output file, to know where to stop
    //fprintf(output, "%d %d\n", bytes, bits_left);
    block_length_index = output_index;

    output_index += 9;          // reserve 5 bytes for length information;

    i = 0;
    BYTE buffer = 0;
    int k;
    int unused = 8;         // unused bits in buffer.
    BYTE mask = 0;          // mask to store read bits.
    int already_read = 0;   // number of bits already written.

    data_index = output_index;      // save the pointer to the beggining of the data
                                    // used for length calculation later

    for ( i = 0; i < _len(stream); i++)
    {
        int len = strlen(codes[stream[i]]);
        already_read = 0;

/* ============================================================================= */

        while ( len >= unused)
        {
            len -= unused;
            mask = 0;
            for ( k = 0; k < unused; k++)
            {
                mask <<= 1;
                mask += codes[stream[i]][already_read++] == '1' ? 1 : 0;
            }
            buffer |= mask;
            //fprintf(output, "%c", buffer);
            if(output_index >= _len(output))
                _extend(output);

            output[output_index++] = buffer;
             /* losing used informations, and getting ready for next ones */
            unused = 8;  // buffer has been printed out, number of unused bits is 8 again
            buffer = 0;
            mask = 0;
        }

/* ============================================================================= */

        if ( len != 0){  // in case that upper while loop covers all bits
            mask = 0;
            for ( k = 0; k < len; k++)
            {
                mask <<= 1;
                mask += codes[stream[i]][already_read++] == '1' ? 1 : 0;
            }
            mask <<= 8 - len - ( 8 - unused);  // (8 - unused) = used
            buffer |= mask;
            mask = 0;
            unused = 8 - len - ( 8 - unused);
            if ( unused == 0)
            {
                //buffer |= mask;
                //fprintf(output, "%c", buffer);
                output[output_index++] = buffer;
                buffer = 0;
                unused = 8;
            }
        }
    }

    if(output_index >= _len(output))
        _extend(output);

    output[output_index] = buffer;

    *((uint64_t*)(output + block_length_index)) = output_index - data_index;
    output[block_length_index + 8] = 8 - unused; // bytes left

    _setsize(output, output_index + 1);

    /*
    ==========================================================================
    I separated bit-write into two cases. 1) current code ( codes[stream[i]] )
    is bigger than unused room in buffer, and while it is bigger you copy bits
    into buffer and print it out. 2) left bits (from previous loop) are less
    numbered then whole available space, and I took them and shifted into buffer
    next to most-left used bit in buffer, and wait for next bit that will fill
    up my buffer.
    ==========================================================================
    */
    /* ========================= */
    /* OVO ZEZA NEKAD. PAZI!!!!  */
    /* ========================= */

    int proradi;

    for (proradi = 0; proradi < 256; proradi++)
        _del(codes[proradi]);
    _del(codes);


    output_index--;

    _setsize(output, output_index);
    return output;
}


/* write_codes() write codes in code field of tree_node_t recursively, if left add 0, if right add 1 */

/**
* @brief write binary codes
* @param tree root of binary tree without codes
* @return void, but tree now has codes written into.
*
* This function write binary codes in code field of tree_node recursively. If left add zero, if right add one.
*
*/

void write_codes(tree_node_t ** tree)
{
    int i;
    if ((*tree)->left != 0)
    {
        tree_node_t * left = (*tree)->left;
        for(i =0; i<= strlen((*tree)->code); i++)
            left->code[i] = (*tree)->code[i];  // kopiranje codea oca

        strcat(left->code, "0\0");
        write_codes(&left);
    }
    if ((*tree)->right != 0)
    {
        tree_node_t *right = (*tree)->right;
        for(i =0; i<= strlen((*tree)->code); i++)
            right->code[i] = (*tree)->code[i];  // kopiranje codea oca

        strcat(right->code, "1\0");
        write_codes(&right);
    }

}


/* treeHight return hight (depth) of tree recursively */
/* That is the code length of rarest character in input file = ma length */

/**
* @brief tree height
* @param tree root of binary tree
* @return height of binary tree
*
* Gets tree height (depth) recursively.
*
*/
int treeHeight(tree_node_t *tree)
{
    if (tree == 0)
        return -1;

    int left_height = treeHeight(tree->left);
    int right_height = treeHeight(tree->right);

    if (left_height > right_height)
        return left_height + 1;
    else
        return right_height + 1;
}

/* functions for decoding */

/*
Function form_tree(char ** codes) forms Huffman tree based on codes[][], it makes path for every distinct char in the codes[][] by going right if next bit is 1, and going left if next bit is 0
Result of this is Huffman tree. Having this, it is much easier to decompress compressed block.
It returns root of Huffman tree
*/

/**
* @brief forming binary tree
* @param codes byte matrix containing Huffman codes
* @return formed binary tree
*
* Decompressing algorithm uses this function when forming binary tree from byte matrix.
* It uses matrix 256*MAX_SIZE. Each row represent Huffman code of indexed byte.
* It makes path for every row in the codes by going right if next bit is 1, and going left if next bit is 0
*
*/

tree_elem_t * form_tree(BYTE ** codes)
{
    tree_elem_t *root = malloc(sizeof(tree_elem_t));
    tree_elem_t *tekuci = 0;
    int i,j;

    root->left = 0; root->right = 0;
    tekuci = root;

    for ( i = 0; i < 256; i++)
        if (codes[i][0] != '\0')
        {
            for (j = 0; j < strlen(codes[i]); j++)
            {
                BYTE next = codes[i][j];
                if ( next == '0')
                {
                    if (tekuci->left == NULL)
                    {
                        tekuci->left = malloc(sizeof(tree_elem_t));
                        tekuci->left->left = NULL;
                        tekuci->left->right = NULL;
                        tekuci->ch = -1;
                    }
                        tekuci = tekuci->left;
                }
                if (next == '1')
                {
                    if (tekuci->right == NULL)
                    {
                        tekuci->right = malloc(sizeof(tree_elem_t));
                        tekuci->right->left = NULL;
                        tekuci->right->right = NULL;
                        tekuci->ch = -1;
                    }
                        tekuci = tekuci->right;
                }
            }
            tekuci->left = NULL;
            tekuci->right = NULL;
            tekuci->ch = i;
            tekuci = root;
        }
    return root;

}

/**
* @brief binary to decimal number
* @param binary 8 byte long array of zeros and ones, representing one byte binary
* @return byte
*
*/

BYTE transform(BYTE *binary)
{
    int len = strlen(binary);
    BYTE ret = 0;
    BYTE mask = 0x01;
    int i;
    //0100 0100
    for ( i = 0; i < len; i++)
    {
        mask = 0x01;
        ret <<= 1;
        mask &= binary[i];
        ret |= mask;
    }
    return ret;
}

/**
* @brief Analyzing first block of memory
* @param name File name
* @param block block size
* @return size of compressed block memory
*
* Functions does the same thing as Huffman algorithm, just does not return compressed memory block
* but size of one, including table of Huffman codes.
*/


int analyze_huffman(char * name, int block)
{
    FILE * input = fopen(name, "rb");

     BYTE * stream= _new(BYTE, block);

    int ucitano = fread(stream, sizeof(BYTE), block, input);

    _len(stream) = ucitano;

    int i,c, distinct = 0, total=0;
    float *frequences;
    tree_node_t *tree = 0;

    frequences = (float *) calloc(256, sizeof(float));

    for( i = 0; i < block; i++)
        {
                total++;
                frequences[stream[i]]++;
        }

    for (i = 0; i<256; i++)
    {
        if (frequences[i] != 0)
        {
            tree_node_t *new_node;
            frequences[i] = frequences[i]*100 /total;
            distinct++;   // da znam koliko imam clanova u pocetnom treeu zbog formiranja hoffmanovog stabla
            new_node = (tree_node_t *) malloc(sizeof(tree_node_t));
            new_node->frec = frequences[i];
            new_node->ch = i;
            new_node->right = 0;
            new_node->left = 0;
            insert(&tree, new_node);
            free(new_node);
        }
    }

    for (i = 0; i<distinct-1; i++)
    {
        tree_node_t * new_node, *left, *right;
        new_node = (tree_node_t *) malloc(sizeof(tree_node_t));
        left = delete_queue(&tree);
        right = delete_queue(&tree);
        new_node->frec = left->frec + right->frec;
        new_node->left = left;
        new_node->right = right;
        insert(&tree, new_node);
    }

    // stablo je formirano
    tree->code[0] = '\0'; // postavljanje koda korena na prazan string.

    write_codes(&tree);

    tree_node_t *stek = 0, *old_node;
    BYTE ** codes;
    push(&stek, tree);

    codes = (BYTE **) calloc(256, sizeof(BYTE *));

    int height = treeHeight(tree);

    for (i = 0; i < 256; i++)
    {
        codes[i] = (BYTE *) calloc(height, sizeof(BYTE));
        codes[i][0] = '\0';
    }

    while(!stek_empty(stek))
    {

        tree_node_t *next = pop(&stek);

        while(next != 0)
        {
            if ((next->left == 0) && (next->right == 0))
                strcpy(codes[next->ch], next->code);

            if (next->right != 0)
                push(&stek, next->right);

            old_node = next;
            next = next->left;
            free(old_node);
        }

    } // freeing tree, and copying codes into codes[][]

    int bytes = 0, bits_left = 0;

    free_struct(&stek);

    for ( i = 0; i < block; i++)
        bytes += strlen(codes[stream[i]]);

    for ( i = 0; i < 256; i++)
        bytes += strlen(codes[i]);


    bits_left = bytes % 8;      // number of usable bits in last byte

    bytes += 256;

    bytes /= 8;                 // number of bytes in compressed file



    for (i = 0; i < 256; i++)
        free(codes[i]);
    free(codes);

    return bits_left ? bytes + 1 : bytes;

}

/**
* @brief Main function for Huffman algorithm
* @param stream memory block form file
* @return compressed memory block using Huffman algorithm
*
* First of all, algorithm picks up frequencies of each byte of input stream.
* After codes has been written into binary tree, functions calls outputAndFree(), that finally
* return compressed memory block.
*/

BYTE * staticHuffman(BYTE * stream)
{
    int i, distinct = 0, total=0;
    float *frequences;
    tree_node_t *tree = 0;

    frequences = (float *) calloc(256, sizeof(float));
    int c;
    for ( i = 0; i < _len(stream); i++)
        {
                total++;
                frequences[stream[i]]++;
        }

    for (i = 0; i<256; i++)
    {
        if (frequences[i] != 0)
        {
            tree_node_t *new_node;
            frequences[i] = frequences[i]*100 /total;
            distinct++;   // da znam koliko imam clanova u pocetnom treeu zbog formiranja hoffmanovog stabla
            new_node = (tree_node_t *) malloc(sizeof(tree_node_t));
            new_node->frec = frequences[i];
            new_node->ch = i;
            new_node->right = 0;
            new_node->left = 0;
            insert(&tree, new_node);
            free(new_node);
        }
    }

    for (i = 0; i<distinct-1; i++)
    {
        tree_node_t * new_node, *left, *right;
        new_node = (tree_node_t *) malloc(sizeof(tree_node_t));
        left = delete_queue(&tree);
        right = delete_queue(&tree);
        new_node->frec = left->frec + right->frec;
        new_node->left = left;
        new_node->right = right;
        insert(&tree, new_node);
    }

    // stablo je formirano
    tree->code[0] = '\0'; // postavljanje koda korena na prazan string.

    write_codes(&tree);

    free(frequences);
    return outputAndFree(&tree, stream);
}

#define MAX_SIZE 50

/**
* @brief decompressing function
* @param compressed
*/


BYTE * decompress_huffman (BYTE * compressed)
{

    BYTE **codes = NULL;
    int i;
    BYTE * decompressed = _new(BYTE, 2*BLOCK);
    int decompressed_index = 0;
    int compressed_index = 0;


    codes = calloc(256, sizeof(BYTE *));

    for ( i = 0; i < 256; i++)
    {
        codes[i] = calloc(MAX_SIZE, sizeof(BYTE));
        codes[i][0] = '\0';
    }

    BYTE space;
    BYTE c1, c2;  // because of EOF
    while (1)
    {
        c1 = compressed[compressed_index++];
        c2 = compressed[compressed_index++];

        if (( c1 == 255) && (c2 == 255))
            break;
            // kraj tabele
        else
        {
            i = 0;
            codes[c1][i++] = c2;
            while ((c2 = compressed[compressed_index++]) != '\n')
                codes[c1][i++] = c2;
            codes[c1][i] = '\0';
        }
    }

    tree_elem_t *root = form_tree(codes), *tekuci;

    for (i = 0; i < 256; i++)
        free(codes[i]);
    free(codes);

    uint64_t bytes, bits_left;
    BYTE lower_byte, higher_byte;

    bytes = *((uint64_t*)(compressed + compressed_index));
    compressed_index += 8;
    bits_left = compressed[compressed_index++];


    // PROVERI KOLIKO JE BYTES I BITS_LEFT

    i = 0;
    int total = 0;
    BYTE next;
    tekuci = root;
    long j;
    for ( j = 0; j < bytes; j++)
    {
        next = compressed[compressed_index++];

        for ( i = 7; i >= 0; i--)
        {

            if ( next >= (1 << i ) ){
                tekuci = tekuci->right;
                next -= (1 << i );
            }
            else
                tekuci = tekuci->left;

            if ((tekuci->left == NULL) && (tekuci->right == NULL))
            {
                if(decompressed_index >= _len(decompressed))
                    _extend(decompressed);
                decompressed[decompressed_index++] = tekuci->ch;
                //fprintf(decompressed, "%c", tekuci->ch);
                tekuci = root;
                total++;
            }


        }


    }

    next = compressed[compressed_index]; // without ++, because it is the last one
    //fscanf(compressed, "%c", &next);

    for ( i = 7; i >= 8 - bits_left; i--)
        {
            if ( next >= (1 << i) ){
                tekuci = tekuci->right;
                next -= 1<<i;
            }
            else
                tekuci = tekuci->left;

            if ((tekuci->left == NULL) && (tekuci->right == NULL))
            {
                if(decompressed_index >= _len(decompressed))
                    _extend(decompressed);
                decompressed[decompressed_index++] = tekuci->ch;
                //fprintf(decompressed, "%c", tekuci->ch);
                tekuci = root;
                total++;
            }

        }

    _setsize(decompressed, decompressed_index);

    return decompressed;

}







