#include <stdio.h>
#include <stddef.h>
#include <stdint-gcc.h>
#include <stdlib.h>
#include "static_Huffman.h"
#include <string.h>
#define BLOCK 32*1024
#include "memalloc.h"

int main(int argc, char **argv)
{
    // This is not static_Huffman but kind of Deflate.

    char * output_name = argv[3];
    char *input_name = argv[1];

    output_name = strcat(argv[3], ".etf");

    FILE * input = fopen(input_name, "rb");
    BYTE * data;

    data = _new(BYTE, BLOCK);

    int ucitano = fread(data, sizeof(BYTE), BLOCK, input);
    _setsize(data, ucitano);

    BYTE * compressed = staticHuffman(data);

    FILE *output = fopen (output_name, "wb");

    int upisano = fwrite(compressed, sizeof(BYTE), _len(compressed), output);

    fclose ( output);
    fclose(input);

/* ======================================================================= */
/*                          END OF COMPRESSION                             */
/* ======================================================================= */

    FILE * decompressed = fopen("dekompresovano.jpg", "wb");
    output = fopen(output_name, "rb");
    int procitano = fread(compressed, sizeof(BYTE), upisano, output);

    BYTE * dekompresovano = decompress_huffman(compressed);

    fwrite(dekompresovano, sizeof(BYTE), BLOCK, decompressed);
	
	fclose(decompressed);
	free(dekompresovano);
	free(data);
	free(compressed);
	
}
