/**
 * @brief GUI interface procedures
 * @file gui.c
 * @author Nikola Kolevski
 */
#include <stdio.h>
#include <stdlib.h>

/* used for the GUI rendering */
#include <locale.h>
#include <ncurses.h>
#include <menu.h>

/* utility */
#include "memalloc.h"
#include <string.h>

#include "fmanager.h"

static WINDOW *my_menu, *selectedMenu;

/**
 * @brief initialize ncurses mode, and enebling settings for ncurses, including
 * keypad, initialze screen,colors,cursor and so on....
 * also initialize memory for file manager
 */
void initialize(){
    setlocale(LC_ALL, "en_US.utf-8");
    stdscr = initscr();          /* Start curses mode */
    noecho();           /* Don't echo() while we do getch */
    cbreak();
    nonl();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    curs_set(0);

    start_color();
    init_pair(1, COLOR_BLACK, COLOR_RED);
    init_pair(2, COLOR_BLACK, COLOR_GREEN);
    init_pair(3, COLOR_BLACK, COLOR_BLUE);
    init_pair(4, COLOR_BLACK, COLOR_WHITE);

    hidden_filter = do_not_show_hidden;
    initiliaze_list();
}
/**
 * @brief printing all files in current directory, also using some arithemetic
 * for scrolling, and calling functions from file manager
 *
 * @param my_menu, window for files in current directory
 */
void printScreen(void)
{
    int maxx, maxy;
    int x = 2, y = 2, i = 0, fileheight, n, check = 0;
    int highlighted = get_highlighted_index();

    file_t *file = get_files();
    n = (int)_len(file);

    getmaxyx(stdscr, maxy, maxx);
    wresize(my_menu, (int)(maxy - maxy*0.1), (int)(maxx/2));

    fileheight = (int)(maxy - 0.225*maxy);
    if(n > fileheight){
          n = fileheight;
          if (highlighted >= fileheight){
              i = highlighted - fileheight;
              n = fileheight + i + 1;
          }
    }

    if(n == 0) {
        wclear(my_menu);
        mvwprintw(my_menu, y, x, "The directory is empty.");
    }

    box(my_menu, 0, 0);

    mvwprintw(my_menu, 0, 2, get_path());

    for(;i < n;i++){
        if(strncmp((file[i].fname + _len(file[i].fname) - 4), ".sz", 4) == 0) {
            check = 1;
            wattron(my_menu, COLOR_PAIR(4));
        }
        else if(file[i].dir)
            wattron(my_menu,COLOR_PAIR(1)); // if it's a dir, colorize it
        else if(!check)
            wattron(my_menu, COLOR_PAIR(2));


        if(file[i].id == file[highlighted].id) {
            if(file[i].selected)
                mvwprintw(my_menu, y++, x,"\xe2\x9c\x94 %s", file[i].fname);
            else
                mvwprintw(my_menu, y++, x,"    %s", file[i].fname);
        }
        else {
            wattron(my_menu, A_REVERSE);
            if(file[i].selected)
                mvwprintw(my_menu, y++, x,"\xe2\x9c\x94 %s", file[i].fname);
            else
                mvwprintw(my_menu, y++, x,"    %s", file[i].fname);
            wattroff(my_menu,A_REVERSE);
        }

        if(check)
            wattroff(my_menu, COLOR_PAIR(4));
        else if(file[i].dir)
            wattroff(my_menu,COLOR_PAIR(1));
        else if(!check)
            wattroff(my_menu,COLOR_PAIR(2));
        check = 0;

    }

    wrefresh(my_menu);
}

/**
 * @brief printing current selected files on selection window
 *
 * @param selectedMenu, selection Window
 */
void printSelectedFiles(void)
{
    int x = 2, y = 2, maxx, maxy;

    file_t *file = get_selected();

    getmaxyx(stdscr, maxy, maxx);
    wresize(selectedMenu, (int)(maxy - maxy*0.1), (int)(maxx/2));

    box(selectedMenu, 0, 0);
    mvwprintw(selectedMenu, 1, 1, "Selected Files:");

    while(file) {
        wattron(selectedMenu, COLOR_PAIR(3));
        wattron(selectedMenu, A_REVERSE);
        mvwprintw(selectedMenu, y++, x, file->fname);

        file = file->next;
    }

    wattroff(selectedMenu, COLOR_PAIR(3));
    wattroff(selectedMenu, A_REVERSE);
    wrefresh(selectedMenu);
}

/**
 * @brief printing Legend on screen
 */
void printStatusBar(void){
   int maxx, maxy;
   getmaxyx(stdscr, maxy, maxx);
   attron(COLOR_PAIR(1));
   attron(A_REVERSE);
   mvwprintw(stdscr, (int)(maxy - maxy*0.1), 0,
             "Directories are in red");
   wclrtoeol(stdscr);
   attroff(COLOR_PAIR(1));
   attron(COLOR_PAIR(2));
   mvwprintw(stdscr, (int)(maxy - maxy*0.1 +1), 0,
             "Compressible files are in green");
   wclrtoeol(stdscr);
   attroff(COLOR_PAIR(2));


   attron(COLOR_PAIR(4));
   mvwprintw(stdscr, (int)(maxy - maxy*0.1 +2), 0,
             "Compressed files are in white");
   wclrtoeol(stdscr);
   attroff(COLOR_PAIR(4));


   attron(COLOR_PAIR(3));
   mvwprintw(stdscr, (int)(maxy - maxy*0.1), (int)(maxx/2),
             "Press space on file to select it or Decompress (.sz archive)");

   wclrtoeol(stdscr);
   mvwprintw(stdscr, (int)(maxy - maxy*0.1 + 1), (int)(maxx/2),
             "Press character 'c' to compress selected files");

   wclrtoeol(stdscr);
   mvwprintw(stdscr, (int)(maxy - maxy*0.1 + 2), (int)(maxx/2),
             "Press character 'r' to clear selected files");

   wclrtoeol(stdscr);
   attroff(COLOR_PAIR(3));
   attroff(A_REVERSE);
}

char *prompt(const char *message) {
    int maxy, maxx;
    char *input = _new(char, 1000);
    getmaxyx(stdscr, maxy, maxx);

    printScreen();
    printSelectedFiles();

    echo();

    curs_set(1);

    wrefresh(stdscr);

    mvwprintw(stdscr, (int)(maxy - maxy*0.1), (int)(maxx/4), message);
    wgetnstr(stdscr, input, 1000);

    printScreen();
    printSelectedFiles();
    printStatusBar();

    curs_set(0);

    noecho();

    refresh();

    return input;
}

/**
 * @brief Processing user input and calling appropriate functions in file
 * manager
 *
 * @param choice User choice
 * @param selectedMenu, selection wndow
 */
void input(int choice)
{
    switch(choice) {
        case KEY_UP:
        case 'k':
            previous_file();
            break;
        case KEY_DOWN:
        case 'j':
            next_file();
            break;
        case 13:
        case 'l':
            enter_dir();
            break;
        case KEY_BACKSPACE:
        case 'h':
            go_back();
            break;
        case ' ':
            toggle_selected();
            break;
        case 'c':
            clear();
            refresh();
            prepare_compression();
        case 'r':
            dealloc_selected();
            werase(selectedMenu);
            load_files();
            break;
        case 8:
            toggle_hidden();
            break;
    }
}

/**
 * @brief deallocating all windows and ending ncurses mode, also deallocate all
 * memory used in file manager
 *
 * @param current directory window to deallocate
 * @param selection window to deallocate
 */
void dealloc(WINDOW *one, WINDOW *two){
    delwin(one);
    delwin(two);
    dealloc_e();
    endwin();
}

/**
 * @brief main program, where windows are set, and main while loop, which
 * process user input
 *
 * @returns 0 for ending the program normally
 */
/* ============== MAIN PROGRAM ============== */
int main(void)
{
    int ch, maxx, maxy;

    initialize();

    getmaxyx(stdscr, maxy, maxx);
    my_menu = newwin((int)(maxy - maxy*0.05), (int)(maxx/2), 0, 0);
    selectedMenu = newwin((int)(maxy - maxy*0.05), (int)(maxx/2), 0, maxx/2);
    refresh();

    printScreen();
    printSelectedFiles();
    printStatusBar();

    while((ch = wgetch(stdscr)) != 'q') {
        input(ch);

        werase(my_menu);
        werase(selectedMenu);
        printScreen();
        printSelectedFiles();
        printStatusBar();
    }
    dealloc(my_menu, selectedMenu);

    return 0;
}
