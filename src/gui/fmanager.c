/**
 * @brief File management procedures
 * @file fmanager.c
 * @author Nikola Kolevski
 *
 * Tightly coupled with GUI, but GUI independant.
 */

/* file management */
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <stdio.h>
#include <string.h>

#include <ncurses.h>

#include "fmanager.h"
#include "runner.h"
#include "gui.h"
#include "memalloc.h"

static file_t *flist;
static file_t* fselection;
static file_t* fselection_last;
static int highlighted = 0;
static int len_home = 0;
static char* comp_home;
static char* path;

/**
 * @brief Copy a string safely.
 *
 * @param str1 Destination string
 * @param str2 Source string
 *
 * Uses the memalloc allocation to easily expand the string to the desired size.
 */
void safecpy(char **str1, char *str2)
{
    size_t len = strlen(str2);

    if(!(*str1))
        *str1 = _new(char, len);

    if(len >= _len(*str1))
        _setsize(*str1, len + 1);
    strncpy(*str1, str2, len + 1);
}

/**
 * @brief File structure constructor
 *
 * @return The new file structure
 */
file_t* mk_file(void)
{
    file_t *file = _new(file_t, 1);
    file->path = _new(char, 150);
    file->fname = _new(char, 150);

    file->next = NULL;

    return file;
}

/**
 * @brief Deallocation of a file structure
 *
 * @param file File structure to be deallocated
 */
void del_file(file_t *file)
{
    _del(file->path);
    _del(file->fname);

    _del(file);
}

int (*hidden_filter)(const struct dirent*) = NULL;

/**
 * @brief Filter function for hidden files
 *
 * @param ent Dir entry provided by scandir
 *
 * @return true or false as a visibility property
 */
int do_not_show_hidden(const struct dirent *ent) {
    size_t len = strlen(ent->d_name);
    if(len >= 1) {
        // filter out hidden files (\.*)
        if(ent->d_name[0] == '.')
            return 0;
    }
    return 1;
}

/**
 * @brief function to trim path (string) to last slash
 *
 * @param s1 -> string to trim
 */
void trimLastDirectory(char *s1)
{
    size_t len = strlen(s1);
    if(len != 1){ /*Check if in root*/
        while(s1[--len] != '/');
        if(len != 0)
            s1[len] = '\0';
        else
            s1[len + 1] = '\0';
    }
}
/**
 * @brief Main file filter used by scandir
 *
 * @param ent The directory entry on which the filter is called
 *
 * @return true or false as a visibility property
 */
int file_filter(const struct dirent *ent) {
    if(strncmp(ent->d_name, "..", 3) == 0 || strncmp(ent->d_name, ".", 2) == 0)
        return 0;
    else if(hidden_filter)
        return hidden_filter(ent);

    return 1;
}


/**
 * @brief function to change path of current directory
 *
 * @param param_path -> current directory
 */
void change_path(char *param_path) {
    path = param_path;
    load_files();
    highlighted = 0;
}

/**
 * @brief initialize current path, list of files and top directory
 */
void initiliaze_list(void){
    flist = _new(file_t, 10);

    path = _new(char, 6);
    strcpy(path, "/home");
    change_path(path);
    comp_home = _new(char, 10);
}
/**
 * @brief Load files from the provided path
 */
void load_files(void)
{
    int i, fileno, n, status;
    struct dirent **ent;
    char *filepath = _new(char, _len(path));
    file_t *fslist;

    memcpy(filepath, path, _len(path));

    inline int checkFile(char path[]) {
        struct stat s;
        if (stat(path, &s) == 0) {
            if (s.st_mode & S_IFDIR)
                return 0;
            else if (s.st_mode & S_IFREG)
                return 1;
        }

        /* explicit return makes a certian exit path in case of stat(...) call
         * failure */
        return -1;
    }

    n = scandir(filepath, &ent, file_filter, alphasort);

    if (n > 0) {
        flist = _setsize(flist, n);

        for(i = 0, fileno = 0; i < n; i++) {
            _setsize(filepath, _len(path) + strlen(ent[i]->d_name) + 2); // slash + '\0'
            memcpy(filepath, path, _len(path));

            sprintf(filepath, "%s/%s", filepath, ent[i]->d_name);
            status = checkFile(filepath);
            if(status != -1) {
                flist[fileno].dir = !status;

                safecpy(&flist[fileno].fname, ent[i]->d_name);
                safecpy(&flist[fileno].path, filepath);

                flist[fileno].id = ent[i]->d_ino;

                fslist = get_selected();

                flist[fileno].selected = 0;

                while(fslist) {
                    if(flist[fileno].id == fslist->id) {
                        flist[fileno].selected = 1;
                        break;
                    }
                    fslist = fslist->next;
                }

                fileno++;
            }
        }

        flist = _setsize(flist, fileno);
    }
    else
        perror ("");

    _del(filepath);
}

/**
 * @brief Decrement highlight -> index of current highlighted file
 */
void previous_file(void)
{
    if(highlighted <= 0)
        highlighted = _len(flist) - 1;
    else
        highlighted--;
}

/**
 * @brief Increment highlight -> index of current highlighted file
 */
void next_file(void){
    if(highlighted >= _len(flist) - 1)
        highlighted = 0;
    else
        highlighted++;
}

/**
 * @brief Retrieve the list of files in the current directory
 *
 * @return list of files in the current directory
 */
file_t* get_files(void)
{
    return flist;
}

/**
 * @brief Toggle showing of hidden files
 */
void toggle_hidden(void)
{
    if(hidden_filter)
        hidden_filter = NULL;
    else
        hidden_filter = do_not_show_hidden;

    load_files();
}

/**
 * @brief Retrieve the files selected for archieving
 * @return List of selected files
 */
file_t* get_selected(void)
{
    return fselection;
}

/**
 * @brief Selecting current highlighted file, by putting it in selection list
 */
void select_t(void)
{
    if(fselection == NULL) {
        fselection = mk_file();
        fselection_last = fselection;
    } else {
        file_t *p = mk_file();
        fselection_last->next = p;
        fselection_last = fselection_last->next;
    }

    fselection_last->dir = flist[highlighted].dir;

    safecpy(&fselection_last->fname, flist[highlighted].fname);
    safecpy(&fselection_last->path, flist[highlighted].path);

    fselection_last->id = flist[highlighted].id;

    flist[highlighted].selected = 1;
}

/**
 * @brief Deselecting current highlighted file, by deleting it from selection
 * list
 */
void deselect(void)
{
    file_t *cursor = fselection, *last;
    if(cursor->next == NULL)
        len_home = 0;

    while(cursor != NULL) {
        if(cursor->id == flist[highlighted].id) {
            if(fselection == cursor)
                fselection = fselection->next;
            else if(cursor->next == NULL){
                fselection_last = last;
                fselection_last->next = NULL;
            }
            else
                last->next = cursor->next;

            flist[highlighted].selected = 0;
            del_file(cursor);
            break;
        }
        last = cursor;
        cursor = cursor->next;
    }
}

/**
 * @brief function to enter current highlighted directory
 */
void enter_dir(void)
{
    if(flist[highlighted].dir) {
        int len = strlen(path) + strlen(flist[highlighted].fname);
        _setsize(path, len + 2); // slash + '\0'
        if(strlen(path) > 1) {
            sprintf(path, "%s/%s", path, flist[highlighted].fname);
        } else sprintf(path, "/%s", flist[highlighted].fname);
        change_path(path);
    }
}

void prepare_decompression() {
    char *extract_path = _new(char, 100);
    char *call = _new(char, 500);
    file_t *file = &flist[highlighted];

    safecpy(&extract_path, file->path);
    trimLastDirectory(extract_path);

    decompress(file->path);

    endwin();

    sprintf(call, "cd %s; tar -xvf %.*s; rm %.*s", extract_path,
                                            strlen(file->path) - 3, file->path,
                                            strlen(file->path) - 3, file->path);
    system(call);

    _del(call);
    _del(extract_path);

    load_files();
}

/**
 * @brief function to toggle select or deselect files in current directory
 * if it file has extension .sz decompress it
 */
void toggle_selected(void) {
    if(strncmp((flist[highlighted].fname + strlen(flist[highlighted].fname) - 3), ".sz", 4) == 0) {
        prepare_decompression();
    }
    else {
        if(!len_home)
            select_compression_root();

        if(flist[highlighted].selected)
            deselect();
        else
            select_t();
    }
}

/**
 * @brief compressing selected files, first into tar (via system call) and then
 * using the tar archive, compress with LZW or Huffman
 */
void prepare_compression(void)
{
    char *call = _new(char, 500);
    char *outname = prompt("Input the archive file name (or path, up to 1000 chars): ");
    file_t *cursor = fselection;

    sprintf(call, "cd %s; tar -cvf %s/%s.tar", comp_home, comp_home, outname);

    while(cursor) {
        if(_len(call) <= strlen(call) + strlen(cursor->path) - len_home + 1)
            _extend(call);

        sprintf(call + strlen(call), " %s", (cursor->path + len_home + 1));
        cursor = cursor->next;
    }

    _setsize(call, strlen(call) + 1);

    endwin();
    system(call);


    sprintf(call, "%s/%s.tar", comp_home, outname);
    compress(call);

    sprintf(call, "rm %s/%s.tar", comp_home, outname);
    system(call);

    _del(call);

    len_home = 0;
}

/**
 * @brief Deallocating selection list and reseting the flag for directory to put
 * compressed file
 */
void dealloc_selected(void){
    len_home = 0;
    file_t *old;
    while(fselection){
        old = fselection;
        fselection = fselection->next;
        _del(old);
    }
    fselection_last = NULL;
}

/**
 * @brief Deallocate all allocated memory
 */
void dealloc_e(void){
    _del(flist);
    _del(path);
    _del(comp_home);
    dealloc_selected();
}

/**
 * @brief function to return current highlighted file
 *
 * @returns highilghted file
 */
file_t get_highlighted(void) {
    return flist[highlighted];
}

/**
 * @brief function to return current highlighted file index
 *
 * @return index of highlighted file
 */
int get_highlighted_index(void){
    return highlighted;
}

/**
 * @brief trim current directory if flag isn't raised and if in compressed
 * directory
 */
void go_back(void)
{
    if(len_home == 0 || len_home < strlen(flist[highlighted].path) - strlen(flist[highlighted].fname) - 1){
        trimLastDirectory(path);
        change_path(path);
    }
}

/**
 * @brief select directory to put compressed file
 */
void select_compression_root(void){
    if(!len_home){
        safecpy(&comp_home, flist[highlighted].path);
        trimLastDirectory(comp_home);
        len_home = strlen(comp_home);
    }
}

char* get_path(void) {
    return path;
}
