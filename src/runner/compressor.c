#include <stdio.h>
#include "lzw.h"
#include "memalloc.h"
#include "static_Huffman.h"
//#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include "runner.h"

void compress(char *input_name)
{
    FILE *input = fopen(input_name, "rb"), *output;
    byte *in, *temp, *out;
    char *output_name;
    int size;

    fseek(input, 0, SEEK_END);
    size = ftell(input);
    rewind(input);

    output_name = malloc(strlen(input_name) + 3 + 1);
    sprintf(output_name, "%s.sz", input_name);

    in = _new(byte, size);

    output = fopen(output_name, "wb");

    int rsize = fread(in, 1, size, input);
    /*temp = staticHuffman(in);*/
    out = lzw_encode(in, 16);
    fwrite(out, 1, _len(out), output);

    _del(in);
    _del(out);
    /*_del(temp);*/

    fclose(input);
    fclose(output);
}


FILE * compress_fast(char * input_name)
{
    FILE * input = fopen(input_name, "rb"), *output;
    byte * in, *out;
    char * output_name = malloc(strlen(input_name) + 3);
    int size;
    rewind(input);
    fseek(input, 0, SEEK_END);
    size = ftell(input);
    rewind(input);

    sprintf(output_name, "%s.sz", input_name);
    in = _new(byte, size);
    output = fopen(output_name, "wb");

    byte *algorithm = malloc(1);
    *algorithm = 0;
    fwrite(algorithm, 1,1,output);
    free(algorithm);

    int rsize = fread(in, 1, size, input);
    out = staticHuffman(in);
    fwrite(out, 1, _len(out), output);

    _del(in);
    _del(out);
    return output;
}

