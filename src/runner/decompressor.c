#include <stdio.h>
#include "lzw.h"
#include "memalloc.h"
#include "static_Huffman.h"
#include <string.h>
#include <stdlib.h>
#include "runner.h"

FILE * decompress_good(char * input_name, FILE * input)
{
    char * output_name = malloc(strlen(input_name)-3);
    FILE * output;
    byte *in, *out, *temp;
    int size;

    strncpy(output_name, input_name, strlen(input_name)-3);
    output_name[strlen(input_name) - 3] = '\0';
    fopen(output_name, "wb");

    rewind(input);
    fseek(input, 0, SEEK_END);
    size = ftell(input) - 1;
    rewind(input);

    byte * a = _new(byte, 1);
    fread(a, 1,1,input);
    _del(a);
    output = fopen(output_name, "wb");
    in = _new(byte, size);

    fread(in, 1, size, input);
    temp = lzw_decode(in);
    out = decompress_huffman(temp);
    fwrite(out, 1, _len(out), output);

    _del(in);
    _del(out);
    _del(temp);

    fclose(input);

    return output;
}

void decompress(char * input_name)
{
    char * output_name = malloc(strlen(input_name) - 3 + 1);
    FILE * output, *input;
    byte * in, *out, *temp;
    int size;

    strncpy(output_name, input_name, strlen(input_name)-3);

    output_name[strlen(input_name) - 3] = '\0';

    output = fopen(output_name, "wb");
    input = fopen(input_name, "rb");

    fseek(input, 0, SEEK_END);
    size = ftell(input) - 1;
    rewind(input);

    in = _new(byte, size);

    fread(in, 1, size, input);
    out = lzw_decode(in);
    /*out = decompress_huffman(temp);*/
    fwrite(out, 1, _len(out), output);

    _del(in);
    _del(out);
    /*_del(temp);*/

    fclose(input);
    fclose(output);
}
