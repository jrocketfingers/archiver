#ifndef __H_FMANAGER__
#define __H_FMANAGER__

#include <dirent.h>

typedef struct file {
    char *path, *fname;
    ino_t id;
    char dir:1;
    char selected:1;

    struct file *next;
} file_t;

void toggle_hidden(void);
void load_files(void);
void change_path(char *path);
void enter_dir(void);
void toggle_selection(void);
void dealloc_selected(void);
void dealloc_e(void);
void initiliaze_list(void);
void prepare_compression(void);
file_t get_highlighted(void);
int get_highlighted_index(void);
file_t* get_selected(void);
file_t* get_files(void);

/* gui calls */
void previous_file(void);
void next_file(void);
void go_back(void);
void toggle_selected(void);
void select_compression_root(void);

char* get_path(void);

int do_not_show_hidden(const struct dirent *ent);
int (*hidden_filter)(const struct dirent *ent);

#endif
