#ifndef __H_LZW__
#define __H_LZW__

#include <stdint.h>

#define M_CLR 256
#define M_EOD 257
#define M_NEW 258

typedef uint8_t byte;
typedef uint16_t ushort;

/* minimal digraph (used as a DAG) structure with pre-determined edges */
typedef struct  {
    ushort next[256];
} lzw_enc_t;

/* linearized doubly linked list implementation */
typedef struct {
    ushort prev, back;
    byte c;
} lzw_dec_t;

byte *lzw_encode(byte *in, int max_bits);
byte *lzw_decode(byte *in);

#endif