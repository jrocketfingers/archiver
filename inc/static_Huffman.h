//typedef char char;
typedef unsigned char BYTE;

/**
* @struct tree_node
* @brief structure used in compressing Huffman algorithm
* @param frec frequency of each of bytes in memory block
* @param ch byte
* @param next next structure in queue when forming binary tree
* @param left left structure in binary tree
* @param right right structure in binary tree
* @param code Huffman code of each byte
*
* Structure used in compressing algorithm for creating binary tree
* and writing Huffman codes into each of bytes code field.
*/

struct tree_node{
	double frec;
	BYTE ch;
	struct tree_node *next, *left, *right;
	char code[30];
};

typedef struct tree_node tree_node_t;

void insert(tree_node_t ** tree, tree_node_t *ubaci);

tree_node_t * delete_queue(tree_node_t **tree);

void free_struct(tree_node_t **tree);

void push(tree_node_t ** stek, tree_node_t *za_ubaciti);

tree_node_t * pop(tree_node_t ** stek);

int stek_empty(tree_node_t *stek);

BYTE * outputAndFree(tree_node_t **root, BYTE * stream);

void write_codes(tree_node_t ** tree);

int treeHeight(tree_node_t *tree);

BYTE * staticHuffman(BYTE * stream);

BYTE transform(BYTE binary[8]);

/**
* @brief Structure used in decompressing Huffman algorithm
* @param ch current byte
* @param left left structure in binary tree
* @param right right structure in binary tree
*
* This structure is used when forming binary tree
* based on Huffman codes. Tree is needed to easier
* get to correct byte using only zeros and ones from compressed data
* where one means right, and zero means left.
*/

struct tree_elem
{
    BYTE ch;
    struct tree_elem *left, *right;
};

typedef struct tree_elem tree_elem_t;


tree_elem_t * form_tree(BYTE ** codes);

BYTE * decompress_huffman (BYTE * compressed);
