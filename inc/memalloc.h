#ifndef __H_MEMALLOC__
#define __H_MEMALLOC__

#include <stdlib.h>

void *mem_alloc(size_t item_size, size_t n_item);
void *mem_extend(void *m, size_t new_n);
void _clear(void *m);



#define _new(type, n)   mem_alloc(sizeof(type), n)
#define _del(m)         { free((size_t*)(m) - 2); m = 0; }
#define _len(m)         *((size_t*)m - 1)
#define _setsize(m, n)  m = mem_extend(m, n)
#define _extend(m)      m = mem_extend(m, _len(m) * 2)

#endif
